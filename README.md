# Testing Playground
This project is simple shop API application.

## Installation
- Clone this repository (`git clone https://gitlab.com/self2005501/testplayground.git`)
- Run command `cd ./testplayground`
- Run command `docker build . -t test-playground`
- Run command `docker run -d --publish 80:3000 --rm test-playground`
- Open in browser: `http://localhost/api/v1/catalog`

## API
### Shop module

Get catalog of products:
### `/api/v1/catalog`

##### Query params:

###### Filters:
- title (string). Part or full product name
- minPrice (number). Min price of products
- maxPrice (number). Max price of products

###### Pagination:
- page (number). Page number
- limit (number). Products count on page

##### Response:

List of `IProductLight` in `ISuccessResponseContainer` (`ISuccessResponseContainer<IProductLight>`).
Or error in `IFailResponseContainer`.

Get single product:
### `/api/v1/catalog/:uuid`

##### Response:

Single product representation `IProduct` in `ISuccessResponseContainer`.
Or error in `IFailResponseContainer`.

## Interfaces

#### Light product representation:
    interface IProductLight {
        uuid: string;
        title: string;
        price: number;
        specialPrice?: number;
    }

#### Full product representation:
    interface IProduct extends IProductLight {
        uuid: string;
        title: string;
        price: number;
        categories: string[];
        description?: string;
        specialPrice?: number;
    }

#### Success container:
    interface ISuccessResponseContainer<Data> {
        data: Data;
        status: 200;
        success: true;
    }

#### Fail container:
    interface ISuccessResponseContainer<Details> {
        details: Details;
        status: number;
        success: false;
    }
