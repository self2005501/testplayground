FROM node:18.6.0

ADD . /app/
RUN (cd app/; npm install;)

CMD (cd app/; npm start;)
