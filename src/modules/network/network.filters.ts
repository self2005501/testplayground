import { Response } from 'express';
import { Catch, ArgumentsHost, ExceptionFilter } from '@nestjs/common';

import { NetworkError, NetworkUnknownError } from './network.exception';
import { packErrorResponse } from './network.helpers';

@Catch(NetworkError)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: NetworkError<any>, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const status = exception.getStatus();
    const response = ctx.getResponse<Response>();

    const error =
      exception instanceof NetworkError ? exception : new NetworkUnknownError();

    response.status(status).json(packErrorResponse(error));
  }
}
