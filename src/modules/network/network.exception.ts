export class NetworkError<D> extends Error {
  constructor(protected details: D, protected status: number) {
    super('Network error');
  }

  public getDetails() {
    return this.details;
  }

  public getStatus() {
    return this.status;
  }
}

export class NetworkNotFoundError<D> extends NetworkError<D> {
  constructor(protected details: D) {
    super(details, 404);
  }

  public getDetails() {
    return this.details;
  }

  public getStatus() {
    return this.status;
  }
}

export class NetworkUnknownError extends NetworkError<string> {
  constructor() {
    super('Unknown server error', 500);
  }

  public getDetails() {
    return this.details;
  }

  public getStatus() {
    return this.status;
  }
}
