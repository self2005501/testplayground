export interface CommonContainer {
  status: number;
}

export interface SuccessContainer<Data> extends CommonContainer {
  data: Data;
  success: true;
}

export interface ErrorContainer<Details> extends CommonContainer {
  details: Details;
  success: false;
}

export type ResponseContainer<Data, Details = any> =
  | SuccessContainer<Data>
  | ErrorContainer<Details>;
