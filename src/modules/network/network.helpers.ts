import { NetworkError } from './network.exception';
import { SuccessContainer, ErrorContainer } from './network.interface';

export const packSuccessResponse = <Data>(
  data: Data,
): SuccessContainer<Data> => ({
  data,
  status: 200,
  success: true,
});

export const packErrorResponse = <Details = any>(
  error: NetworkError<Details>,
): ErrorContainer<Details> => ({
  success: false,
  status: error.getStatus(),
  details: error.getDetails(),
});
