import {
  Injectable,
  CallHandler,
  NestInterceptor,
  ExecutionContext,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SuccessContainer } from './network.interface';
import { packSuccessResponse } from './network.helpers';

@Injectable()
export class TransformInterceptor<Data>
  implements NestInterceptor<Data, SuccessContainer<Data>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<SuccessContainer<Data>> {
    return next.handle().pipe(map(packSuccessResponse));
  }
}
