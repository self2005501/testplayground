export interface SelectOneQueryParams {
  uuid: string;
}

export interface SelectAllQueryParams {
  page?: string;
  limit?: string;
  title?: string;
  minPrice?: string;
  maxPrice?: string;
}

export interface PaginationParams {
  page: number;
  limit: number;
}

export interface ProductsFilters {
  title?: string;
  minPrice?: number;
  maxPrice?: number;
}

export type SelectAllParams = ProductsFilters & PaginationParams;

export interface ProductLight {
  uuid: string;
  title: string;

  price: number;
  specialPrice?: number;
}

export interface Product extends ProductLight {
  description: string;
  categories: string[];
}

export type ProductsMap = Record<string, Product>;

export interface ProductsResponseData {
  page: number;
  limit: number;
  pages: number;
  list: ProductLight[];
}
