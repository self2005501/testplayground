import { SelectAllParams, SelectAllQueryParams } from './shop.interface';

const DEFAULT_LIMIT = 10;
const INCORRECT_NUMBER_MESSAGE_TEMPLATE = `Incorrect number`;

const parseNumber = (value: string) => {
  const result = Number.parseInt(value);

  if (isNaN(result)) {
    throw new Error(INCORRECT_NUMBER_MESSAGE_TEMPLATE);
  }

  if (!isFinite(result)) {
    throw new Error(INCORRECT_NUMBER_MESSAGE_TEMPLATE);
  }

  return result;
};

export const translateQueryToSelectAllParams = (
  params: SelectAllQueryParams,
): SelectAllParams => {
  const page = params.page ? parseNumber(params.page) : 0;
  const limit = params.limit ? parseNumber(params.limit) : DEFAULT_LIMIT;

  const result: SelectAllParams = {
    page,
    limit,
  };

  if (params.title) {
    result.title = params.title;
  }

  if (params.minPrice) {
    result.minPrice = parseNumber(params.minPrice);
  }

  if (params.maxPrice) {
    result.maxPrice = parseNumber(params.maxPrice);
  }

  return result;
};
