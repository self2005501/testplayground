import {
  easeProducts,
  filterProducts,
  applyPagination,
  generateProducts,
  calculatePagesCount,
} from './shop.helpers';
import {
  Product,
  SelectAllParams,
  ProductsResponseData,
} from './shop.interface';

export class ShopService {
  private products = generateProducts(100);

  public selectAll(props: SelectAllParams): ProductsResponseData {
    const products = Object.values(this.products);

    const filteredProducts = filterProducts(products, props);

    return {
      page: props.page,
      limit: props.limit,
      list: easeProducts(applyPagination(filteredProducts, props)),
      pages: calculatePagesCount(filteredProducts.length, props.limit),
    };
  }

  public selectOne(uuid: string): Product | null {
    const result = this.products[uuid];
    return result || null;
  }
}
