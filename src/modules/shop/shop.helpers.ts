import { randomUUID } from 'crypto';

import {
  Product,
  ProductsMap,
  ProductLight,
  ProductsFilters,
  PaginationParams,
} from './shop.interface';

export const generateProduct = (index: number): Product => {
  const price = index * 10;

  const common = {
    price,
    categories: [],
    uuid: randomUUID(),
    title: `Product-${index}`,
    description: `Description for product Product-${index}`,
  };

  return index % 2 === 0 ? common : { ...common, specialPrice: price / 2 };
};

export const generateProductsMap = (count = 100) =>
  new Array(count).fill(null).reduce<ProductsMap>((result, _, index) => {
    const product = generateProduct(index);
    result[product.uuid] = product;
    return result;
  }, {});

export const createProductsGenerator = () => {
  let cache: ProductsMap | null = null;

  return (count: number) => {
    if (!cache) {
      cache = generateProductsMap(count);
    }

    return cache;
  };
};

export const generateProducts = createProductsGenerator();

export const applyPagination = (
  products: Product[],
  { page, limit }: PaginationParams,
): Product[] => {
  const start = page * limit;
  const end = start + limit;

  return products.slice(start, end);
};

export const filterProducts = (
  products: Product[],
  filters: ProductsFilters,
): Product[] =>
  products.filter((product) => {
    const minPrice = Math.max(filters.minPrice || 0, 0);

    if (product.price < minPrice) {
      return false;
    }

    const maxPrice =
      filters.maxPrice === 0 ? filters.maxPrice : filters.maxPrice || Infinity;

    if (product.price > maxPrice) {
      return false;
    }

    return filters.title
      ? product.title.toLowerCase().includes(filters.title.toLowerCase())
      : true;
  });

export const easeProducts = (products: Product[]): ProductLight[] =>
  products.map((product) => ({
    uuid: product.uuid,
    title: product.title,
    price: product.price,
    specialPrice: product.specialPrice,
  }));

export const calculatePagesCount = (count: number, limit: number) => {
  if (limit === 0) {
    return 0;
  }

  return Math.ceil(count / limit);
};
