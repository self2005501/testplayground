import { Controller, Get, Param, Query } from '@nestjs/common';

import { NetworkNotFoundError } from '../network/network.exception';

import { ShopService } from './shop.service';
import { translateQueryToSelectAllParams } from './shop.translators';
import { SelectAllQueryParams, SelectOneQueryParams } from './shop.interface';

@Controller({ path: 'catalog', version: '1' })
export class ShopController {
  constructor(private shopService: ShopService) {}

  @Get()
  selectAll(@Query() query: SelectAllQueryParams) {
    const params = translateQueryToSelectAllParams(query);
    return this.shopService.selectAll(params);
  }

  @Get(':uuid')
  selectOne(@Param() params: SelectOneQueryParams) {
    const product = this.shopService.selectOne(params.uuid);

    if (!product) {
      throw new NetworkNotFoundError('Product not found');
    }

    return product;
  }
}
