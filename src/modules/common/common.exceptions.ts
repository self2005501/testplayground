const DEFAULT_MESSAGE = 'UNKNOWN ERROR';

interface CommonExceptionData {
  message: string;
  type: string | null;
}

export class ControllerError extends Error {
  constructor(
    public message = DEFAULT_MESSAGE,
    public type: string | null = null,
  ) {
    super(message);
  }

  public getData(): CommonExceptionData {
    return { message: this.message, type: this.type };
  }

  public getMessage() {
    return this.getData().message;
  }

  public getType() {
    return this.getData().type || null;
  }
}
