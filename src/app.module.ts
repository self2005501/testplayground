import { Module } from '@nestjs/common';

import { AppService } from './app.service';
import { AppController } from './app.controller';

import { ShopModule } from './modules/shop/shop.module';

@Module({
  imports: [ShopModule],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}
